export class User {
    displayName: string;
    email: string;
    phoneNumber: number;
    photoURL: string;
    providerId: string;
    uid: string;

    constructor(parameters) {

    }
}