import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from "@ionic/storage";
import { Facebook } from '@ionic-native/facebook';
import { OneSignal } from '@ionic-native/onesignal';
import { TextToSpeech } from "@ionic-native/text-to-speech";
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserDataProvider } from '../providers/user-data/user-data';

export const config = {
  apiKey: "AIzaSyDQwl3CEpMNAryvazLql3EFLS7LIoIHfuQ",
  authDomain: "ionic-blogs.firebaseapp.com",
  databaseURL: "https://ionic-blogs.firebaseio.com",
  projectId: "ionic-blogs",
  storageBucket: "ionic-blogs.appspot.com",
  messagingSenderId: "592558745872"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Facebook,
    OneSignal,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TextToSpeech,
    UserDataProvider
  ]
})
export class AppModule { }
