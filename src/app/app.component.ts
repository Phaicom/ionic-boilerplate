import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import { TextToSpeech } from "@ionic-native/text-to-speech";

import { LOGIN_PAGE, MENUS_PAGE, PROFILE_PAGE } from '../pages/pages.constants';
import { UserDataProvider } from '../providers/user-data/user-data';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  constructor(
    private userData: UserDataProvider,
    public events: Events,
    public storage: Storage,
    public platform: Platform,
    public toastCtrl: ToastController,
    private oneSignal: OneSignal,
    private tts: TextToSpeech,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {
    this.initializeApp();
    this.rootPage = LOGIN_PAGE;
    this.listenToLoginEvents();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.oneSignal.startInit('fe1378b5-0c13-4206-a5e3-6b0adc9ada6c', '592558745872 ');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe((res) => {
        // do something when notification is received
        console.log(res);
      });

      this.oneSignal.handleNotificationOpened().subscribe((res) => {
        // do something when a notification is opened
        console.log(res);
        this.presentToast(res.notification.payload.title);
      });

      this.oneSignal.endInit();
    });
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.openPage(MENUS_PAGE)

    });

    this.events.subscribe('user:logout', () => {
      this.openPage(LOGIN_PAGE)
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page);
  }

  presentToast(title: string) {
    let msg = `User was read ${title} successfully`;

    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    this.tts.speak(msg)
      .then(() => {
        toast.present();
      })
      .catch((reason: any) => console.log(reason));
  }
}
