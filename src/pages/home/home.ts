import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Storage } from '@ionic/storage';

import { User } from '../../models/user';
import { Blog } from '../../models/blog';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  blogListRef: any;
  blogList: any;
  user: User = {
    displayName: '',
    email: '',
    phoneNumber: 0,
    photoURL: 'http://via.placeholder.com/150x150',
    providerId: '',
    uid: ''
  }

  constructor(
    public fireStore: AngularFirestore,
    public alertCtrl: AlertController,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.blogListRef = this.fireStore.collection<Blog>(`/blogList`);
    this.blogList = this.blogListRef.valueChanges();

    this.storage.get('user').then(user => {
      this.user = user;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  addBlog() {
    const alert = this.alertCtrl.create({
      title: 'New Blog',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
        {
          name: 'content',
          placeholder: 'Content'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            this.blogListRef.add({
              title: data.title,
              content: data.content,
              date: new Date(),
              auther: this.user.displayName,
              uid: this.user.uid
            });
          }
        }
      ]
    });
    alert.present();
  }

}
