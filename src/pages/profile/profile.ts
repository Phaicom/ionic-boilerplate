import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Storage } from '@ionic/storage';

import { User } from '../../models/user';
import { Blog } from '../../models/blog';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  blogListRef: any;
  blogList: any;
  user: User = {
    displayName: '',
    email: '',
    phoneNumber: 0,
    photoURL: 'http://via.placeholder.com/150x150',
    providerId: '',
    uid: ''
  }

  constructor(
    public fireStore: AngularFirestore,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.blogListRef = this.fireStore.collection<Blog>(`/blogList`);
    this.blogList = this.blogListRef.valueChanges();

    this.storage.get('user').then(user => {
      this.user = user;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
