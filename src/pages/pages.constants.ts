export const MENUS_PAGE = "MenusPage";
export const HOME_PAGE = "HomePage";
export const BOOKMARK_PAGE = "BookmarkPage";
export const LOGIN_PAGE = "LoginPage";
export const PROFILE_PAGE = "ProfilePage";