import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { User } from '../../models/user';
import { HOME_PAGE, BOOKMARK_PAGE, PROFILE_PAGE } from '../pages.constants';
import { UserDataProvider } from '../../providers/user-data/user-data';

@IonicPage()
@Component({
  selector: 'page-menus',
  templateUrl: 'menus.html',
})
export class MenusPage {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HOME_PAGE;
  pages: Array<{ title: string, component: any }>;
  user: User = {
    displayName: '',
    email: '',
    phoneNumber: 0,
    photoURL: 'http://via.placeholder.com/150x150',
    providerId: '',
    uid: ''
  }

  constructor(
    private userData: UserDataProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HOME_PAGE },
      { title: 'Bookmark', component: BOOKMARK_PAGE }
    ];

    this.storage.get('user').then(user => {
      this.user = user;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenusPage');
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(): void {
    this.userData.logout();
  }

  openProfilePage(): void {
    this.navCtrl.push(PROFILE_PAGE);
  }

}
